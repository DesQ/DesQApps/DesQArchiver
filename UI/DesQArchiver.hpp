/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// Local Headers
#include <desqui/MainWindow.hpp>

class ArchiveModel;

class DesQArchiver : public DesQUI::MainWindow {
    Q_OBJECT

    public:
        DesQArchiver();

        /* mode ∈ [ create, extract,  auto ] */
        void load( QString mode, QStringList files );

    private:
        /* Variables */
        QStackedWidget *pages;

        void createUI();
        void setWindowProperties();

        void buildActions();

    /* Private variables and Slots */
    private:
        /* Handle the various actions emitted from DesQActionBar */
        void handleAction( QString path );

        /* Update actions */
        void updateActions();

        /* Extract existing archive */
        void extract();

        /* Compress the current files */
        void compress();

        /* Open an archive for viewing */
        void open();

        /* Add files/folders to an archive */
        void addFiles();
        void addFolders();

        /* Delete files from an archive */
        void remove();

        /* A generic scroll to handle page overflows */
        QTreeView *view;
        ArchiveModel *model;

        QProgressBar *progressBar;

        bool mExists = false;
        QString archiveName;

        QMap<int, QList<int> > actions;
};
