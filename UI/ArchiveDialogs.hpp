/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

class NewArchiveDialog : public QDialog {
    Q_OBJECT

    public:
        static QString newArchive( QWidget * );

    private:
        NewArchiveDialog( QWidget * );
        void createGUI();

        QString filename, location, format;
        QLineEdit *locationLE;
        QString archive();

    private Q_SLOTS:
        void updateFileName( QString );
        void updateDirName();
        void updateFormat( QString );
};

typedef struct ExtractionSummary_t {
    QString destination = "";
    bool    openDest    = false;
    bool    closeDA     = true;
    bool    savePaths   = true;
    bool    allFiles    = false;
} ExtractionSummary;

class ArchiveExtractDialog : public QDialog {
    Q_OBJECT

    public:
        static ExtractionSummary * extractArchive( QWidget *, QString );

    private:
        ArchiveExtractDialog( QWidget *, QString );
        void createGUI();

        QLineEdit *locationLE;

        QCheckBox *subFolderCB;
        QLineEdit *subFolderLE;

        QCheckBox *openDestCB;
        QCheckBox *closeWinCB;
        QCheckBox *preserveCB;

        QRadioButton *selectedRB;
        QRadioButton *allFilesRB;

        QPushButton *extractBtn;

    private Q_SLOTS:
        void updateDirName();
};
