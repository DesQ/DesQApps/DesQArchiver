/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "DesQArchiver.hpp"
#include "ArchiveModel.hpp"
#include "ArchiveDialogs.hpp"

// DesQ Headers
#include <desq/Utils.hpp>

const QStringList archiveMimes = {
    "application/vnd.ms-cab-compressed", "application/x-7z-compressed",       "application/x-7z-compressed-tar",   "application/x-ace",
    "application/x-alz",                 "application/x-ar",                  "application/x-arj",                 "application/x-bzip",
    "application/x-bzip2",               "application/x-bzip-compressed-tar", "application/x-cbr",                 "application/x-cbz",
    "application/x-cd-image",            "application/x-compress",            "application/x-compressed-tar",      "application/x-cpio",
    "application/x-deb",                 "application/x-ear",                 "application/x-gzip",                "application/x-java-archive",
    "application/x-lha",                 "application/x-lzip",                "application/x-lzip-compressed-tar", "application/x-lzma",
    "application/x-lzma-compressed-tar", "application/x-lzop",                "application/x-lzop-compressed-tar",
    "application/x-ms-dos-executable",   "application/x-rar",                 "application/x-rpm",                 "application/x-rzip",
    "application/x-stuffit",             "application/x-tar",                 "application/x-tarz",                "application/x-war",
    "application/x-xz",                  "application/x-xz-compressed-tar",   "application/x-zoo",                 "application/zip"
};

static bool isArchive( QString path ) {
    if ( not DesQ::Utils::isFile( path ) ) {
        return false;
    }

    return archiveMimes.contains( DesQ::Utils::getMimeType( path ) );
}


DesQArchiver::DesQArchiver() : DesQUI::MainWindow( "Archiver" ) {
    createUI();
    setWindowProperties();

    setMinimumWidth( 300 );

    updateActions();
}


void DesQArchiver::load( QString mode, QStringList files ) {
    /**
     * mode ∈ [ create, extract,  auto ]
     *	create  -> Create a new archive
     *	extract -> Extract the existing archive
     *	auto    -> Extract if files[ 0 ] is an existing archive, create otherwise.
     *
     * files -> List of files which need to be added/extracted
     *	files[ 0 ] -> Will always be the archive name
     **/

    archiveName = files.takeAt( 0 );

    if ( mode == "auto" ) {
        qDebug() << "Detecting mode";

        if ( isArchive( archiveName ) ) {
            mode = "extract";
        }

        else {
            mode = "create";
        }
    }

    if ( mode == "create" ) {
        model->growBranch( files );
        mExists = false;
    }

    else {
        /* Ensure that this file actually exists!! */
        if ( DesQ::Utils::isFile( archiveName ) ) {
            model->loadArchive( archiveName );
            mExists = true;
            // selectBranches( files );
        }

        /* Automatically goes to create mode!! */
        else {
            mExists = false;
        }
    }

    updateActions();
}


void DesQArchiver::createUI() {
    view = new QTreeView( this );
    view->setIconSize( QSize( 32, 32 ) );
    view->setFrameStyle( QFrame::NoFrame );

    progressBar = new QProgressBar();
    progressBar->setFixedHeight( 2 );
    progressBar->setRange( 0, 100 );
    progressBar->setFormat( "" );

    QWidget *base = new QWidget();

    QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setContentsMargins( QMargins( 46, 0, 0, 0 ) );
    lyt->setSpacing( 0 );

    lyt->addWidget( view );
    lyt->addWidget( progressBar );
    base->setLayout( lyt );

    setMainView( base );

    model = new ArchiveModel();
    view->setModel( model );

    view->setSelectionBehavior( QAbstractItemView::SelectRows );
    view->setSelectionMode( QAbstractItemView::ExtendedSelection );

    view->header()->setStretchLastSection( false );
    view->header()->setSectionResizeMode( 0, QHeaderView::Stretch );
    view->header()->setSectionResizeMode( 1, QHeaderView::ResizeToContents );

    connect(
        model, &ArchiveModel::archiveLoading, [ = ]() {
            progressBar->setRange( 0, 0 );
        }
    );

    connect(
        model, &ArchiveModel::archiveLoaded, [ = ]() {
            progressBar->setRange( 0, 100 );
            progressBar->setValue( 0 );
        }
    );

    /* Build the ActionBar */
    buildActions();

    /* Updating the actions */
    connect( view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &DesQArchiver::updateActions );
}


void DesQArchiver::buildActions() {
    /** Create the ActionBar */
    createActionBar( Qt::Vertical );

    /* Remember the actions */
    actions[ 0 ] = QList<int>();

    int idx;

    // Page #0
    idx = addAction(
        0,
        "Open", QIcon::fromTheme( "document-open" ),
        "open", "Open archive",
        false, false
    );
    actions[ 0 ].append( idx );

    addSpace( 0, false );

    idx = addAction(
        0,
        "Extract", QIcon::fromTheme( "archive-extract" ),
        "extract", "Extract archive",
        false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction(
        0,
        "Compress", QIcon::fromTheme( "application-x-compressed-tar" ),
        "compress", "Create archive",
        false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction(
        0,
        "Preview", QIcon::fromTheme( "document-preview-archive" ),
        "preview", "Preview selected file",
        false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction(
        0,
        "Find", QIcon::fromTheme( "edit-find" ),
        "search", "Find a member",
        false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction( 0,
                     "Add Files", QIcon::fromTheme( "archive-insert" ),
                     "add-files", "Add files to archive",
                     false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction( 0,
                     "Add Folder", QIcon::fromTheme( "folder-add" ),
                     "add-folder", "Add a folder to the archive",
                     false, false
    );
    actions[ 0 ].append( idx );

    idx = addAction(
        0,
        "Remove", QIcon::fromTheme( "archive-remove" ),
        "remove", "Remove files from\tarchive",
        false, false
    );
    actions[ 0 ].append( idx );

    /* Disable all actions except opening an archive and add files */
    setActionDisabled( 0, actions.value( 0 ).value( 1 ) );          // Extract
    setActionDisabled( 0, actions.value( 0 ).value( 2 ) );          // Compress
    setActionDisabled( 0, actions.value( 0 ).value( 3 ) );          // Preview
    setActionDisabled( 0, actions.value( 0 ).value( 4 ) );          // Find
    setActionDisabled( 0, actions.value( 0 ).value( 5 ) );          // Add files
    setActionDisabled( 0, actions.value( 0 ).value( 6 ) );          // Add folders
    setActionDisabled( 0, actions.value( 0 ).value( 7 ) );          // Delete

    connect( this, &DesQUI::MainWindow::actionRequested, this, &DesQArchiver::handleAction );
}


void DesQArchiver::setWindowProperties() {
    setAppTitle( "DesQ Archiver" );
    setAppIcon( QIcon( ":/icons/desq-archiver.png" ) );

    setMinimumSize( 600, 400 );
    resize( 800, 600 );
}


void DesQArchiver::handleAction( QString action ) {
    if ( action == "extract" ) {
        extract();
    }

    if ( action == "compress" ) {
        compress();
    }

    else if ( action == "preview" ) {
        // preview();
    }

    else if ( action == "open" ) {
        open();
    }

    else if ( action == "search" ) {
        // search();
    }

    else if ( action == "add-files" ) {
        addFiles();
    }

    else if ( action == "add-folder" ) {
        addFolders();
    }

    else if ( action == "remove" ) {
        remove();
    }
}


void DesQArchiver::updateActions() {
    /* This archive exists, enable extraction */
    if ( mExists ) {
        setActionEnabled( 0, actions.value( 0 ).value( 1 ) );           // Extract
        setActionDisabled( 0, actions.value( 0 ).value( 2 ) );          // Compress
        setActionDisabled( 0, actions.value( 0 ).value( 3 ) );          // Preview
        setActionDisabled( 0, actions.value( 0 ).value( 4 ) );          // Find
        setActionDisabled( 0, actions.value( 0 ).value( 5 ) );          // Add files
        setActionDisabled( 0, actions.value( 0 ).value( 6 ) );          // Add folders
        setActionDisabled( 0, actions.value( 0 ).value( 7 ) );          // Delete
    }

    else {
        setActionDisabled( 0, actions.value( 0 ).value( 1 ) );          // Extract

        if ( not model->rowCount() ) {
            setActionDisabled( 0, actions.value( 0 ).value( 2 ) );          // Compress
        }
        else {
            setActionEnabled( 0, actions.value( 0 ).value( 2 ) );           // Compress
        }

        setActionDisabled( 0, actions.value( 0 ).value( 3 ) );          // Preview
        setActionDisabled( 0, actions.value( 0 ).value( 4 ) );          // Find
        setActionEnabled( 0, actions.value( 0 ).value( 5 ) );           // Add files
        setActionEnabled( 0, actions.value( 0 ).value( 6 ) );           // Add folders

        if ( view->selectionModel()->selectedIndexes().count() ) {
            setActionEnabled( 0, actions.value( 0 ).value( 7 ) );           // Delete
        }
        else {
            setActionDisabled( 0, actions.value( 0 ).value( 7 ) );          // Delete
        }
    }
}


void DesQArchiver::extract() {
    ExtractionSummary *summary = ArchiveExtractDialog::extractArchive( this, archiveName );

    if ( not summary ) {
        return;
    }

    LibArchiveQt *arc = new LibArchiveQt( archiveName );

    arc->setDestination( summary->destination );

    progressBar->setRange( 0, 100 );
    connect( arc, &LibArchiveQt::progress, progressBar, &QProgressBar::setValue );

    setDisabled( true );

    if ( summary->allFiles ) {
        connect(
            arc, &LibArchiveQt::jobComplete, [ = ]() {
                setEnabled( true );
                progressBar->setValue( 0 );
                QProcess::startDetached(
                    "notify-send", {
                        "-i", "desq-archiver", "-a", "DesQ Archiver",
                        "-t", "2000", "Exrtaction successful",
                        "The archive has been extracted successfully."
                    }
                );

                if ( summary->openDest ) {
                    QProcess::startDetached( "pcmanfm-qt", { summary->destination } );
                }

                if ( summary->closeDA ) {
                    close();
                }
            }
        );

        connect(
            arc, &LibArchiveQt::jobFailed, [ = ]() {
                setEnabled( true );
                progressBar->setValue( 0 );
                QMessageBox::critical(
                    this,
                    "DesQ Archiver | Extraction failed!",
                    "DesQ Archiver encountered some errors while extracting the archive. "
                    "Changes you may want to review the extracted files before closing the archive."
                );

                if ( summary->openDest ) {
                    QProcess::startDetached( "pcmanfm-qt", { summary->destination } );
                }
            }
        );

        arc->extractArchive();
    }

    else {
        QModelIndexList selection = view->selectionModel()->selectedRows( 0 );
        int             failed    = 0;
        connect( arc, &LibArchiveQt::jobFailed, [ = ]() mutable {
                     failed++;
                 }
        );

        for ( int i = 0; i < selection.count(); i++ ) {
            QModelIndex idx        = selection[ i ];
            QString     memberPath = model->nodePath( idx );
            arc->extractMember( memberPath );
            arc->waitForFinished();
            progressBar->setValue( (int)(100 * i / selection.count() ) );
        }

        setEnabled( true );
        progressBar->setValue( 0 );

        if ( failed ) {
            QMessageBox::critical(
                this,
                "DesQ Archiver | Extraction failed!",
                "DesQ Archiver encountered some errors while extracting the archive. "
                "Changes you may want to review the extracted files before closing the archive."
            );

            if ( summary->openDest ) {
                QProcess::startDetached( "pcmanfm-qt", { summary->destination } );
            }
        }

        else {
            QProcess::startDetached(
                "notify-send", {
                    "-i", "desq-archiver", "-a", "DesQ Archiver",
                    "-t", "2000", "Exrtaction successful",
                    "The archive has been extracted successfully."
                }
            );

            if ( summary->openDest ) {
                QProcess::startDetached( "pcmanfm-qt", { summary->destination } );
            }

            if ( summary->closeDA ) {
                close();
            }
        }
    }
}


void DesQArchiver::compress() {
    archiveName = NewArchiveDialog::newArchive( this );

    if ( archiveName.isEmpty() ) {
        return;
    }

    LibArchiveQt *arc = new LibArchiveQt( archiveName );
    connect(
        arc, &LibArchiveQt::jobComplete, [ = ]() {
            setEnabled( true );
            progressBar->setValue( 0 );
            QProcess::startDetached(
                "notify-send", {
                    "-i", "desq-archiver", "-a", "DesQ Archiver",
                    "-t", "2000", "Archiving successful",
                    "The archive has been created successfully."
                }
            );
        }
    );

    connect(
        arc, &LibArchiveQt::jobFailed, [ = ]() {
            setEnabled( true );
            progressBar->setValue( 0 );
            QMessageBox::critical(
                this,
                "DesQ Archiver | Archiving failed!",
                "DesQ Archiver encountered some errors while creating the archive. "
                "Changes you made to the archive are not saved."
            );
        }
    );

    progressBar->setRange( 0, 100 );
    connect( arc, &LibArchiveQt::progress, progressBar, &QProgressBar::setValue );

    arc->updateInputFiles( model->filePaths(), LibArchiveQt::CommonRelativePath );

    setDisabled( true );
    arc->createArchive();
}


void DesQArchiver::open() {
    QString path = DesQ::Utils::dirName( archiveName );

    if ( path.isEmpty() or not DesQ::Utils::isDir( path ) ) {
        path = QDir::currentPath();
    }

    archiveName = QFileDialog::getOpenFileName(
        this,
        "DesQ Archiver | Open Archive",
        path,
        "Archives (*.zip *.tar.xz *.tar.gz *.tar.bz2 *.txz *.tgz *.tbz2 *.7z);;All files (*.*)"
    );

    if ( DesQ::Utils::isFile( archiveName ) ) {
        model->loadArchive( archiveName );
        mExists = true;
        // selectBranches( files );
    }
}


void DesQArchiver::addFiles() {
    QStringList files = QFileDialog::getOpenFileNames(
        this,
        "DesQ Archiver | Choose files",
        QDir::currentPath()
    );

    model->growBranch( files );
    updateActions();
}


void DesQArchiver::addFolders() {
    QString loc = QFileDialog::getExistingDirectory(
        this,
        "DesQ Archiver | Choose directory",
        QDir::currentPath()
    );

    if ( DesQ::Utils::exists( loc ) ) {
        model->growBranch( { loc, } );
    }

    updateActions();
}


void DesQArchiver::remove() {
    QModelIndexList selection = view->selectionModel()->selectedRows( 0 );

    if ( selection.count() ) {
        model->chopBranch( selection );
    }

    updateActions();
}
