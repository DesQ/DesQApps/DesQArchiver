/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ArchiveDialogs.hpp"
#include <libarchiveqt.h>
#include <desq/Utils.hpp>

/**
 * New Archive Dialog
 **/
QString NewArchiveDialog::newArchive( QWidget *parent ) {
    NewArchiveDialog *dlg = new NewArchiveDialog( parent );

    if ( dlg->exec() == QDialog::Accepted ) {
        return dlg->archive();
    }

    else {
        return QString();
    }
}


NewArchiveDialog::NewArchiveDialog( QWidget *parent ) : QDialog( parent ) {
    format = ".tar.xz";
    createGUI();

    setMinimumWidth( 450 );
}


void NewArchiveDialog::createGUI() {
    setWindowTitle( "DesQ Archiver | Create an Archive" );
    setWindowIcon( QIcon( ":/icons/desq-archiver.png" ) );

    /* Name Lyt */
    QLineEdit *nameLE = new QLineEdit( this );
    nameLE->setPlaceholderText( "Type the archive name here" );
    connect( nameLE, &QLineEdit::textChanged, this, &NewArchiveDialog::updateFileName );

    QLabel *nameLbl = new QLabel( "Archive &Name:" );
    nameLbl->setBuddy( nameLE );

    QHBoxLayout *nLyt = new QHBoxLayout();
    nLyt->addWidget( nameLbl );
    nLyt->addWidget( nameLE );

    /* Location Lyt */
    QLabel *locationLbl = new QLabel( "&Location:", this );

    locationLE = new QLineEdit( this );
    locationLE->setText( QDir::currentPath() );
    locationLE->setToolTip( "Click browse to select where the archive will be stored" );
    locationLE->setDisabled( true );
    locationLbl->setBuddy( locationLE );

    QToolButton *locationTB = new QToolButton( this );
    locationTB->setIcon( QIcon::fromTheme( "folder" ) );
    locationLE->setToolTip( "Click to select where the archive will be stored" );
    connect( locationTB, &QToolButton::clicked, this, &NewArchiveDialog::updateDirName );

    QHBoxLayout *lLyt = new QHBoxLayout();
    lLyt->addWidget( locationLbl );
    lLyt->addWidget( locationLE );
    lLyt->addWidget( locationTB );

    /* Format Layout */
    QComboBox *formatsCB = new QComboBox( this );
    formatsCB->addItems( QStringList() << ".tar.xz" << ".tar.gz" << ".tar.bz2" << ".7z" << ".zip" );
    formatsCB->addItems( QStringList() << ".tar" << ".iso" << ".cpio" << ".shar" << ".tar.Z" << ".ar" << ".xar" );
    connect( formatsCB, &QComboBox::currentTextChanged, this, &NewArchiveDialog::updateFormat );

    QLabel *formatsLbl = new QLabel( "&Format:", this );
    formatsLbl->setBuddy( formatsCB );

    /* WHAT IS ADVACNED? WHY DID I ADD THIS? */
    QCheckBox *advanced = new QCheckBox( "Ad&vanced", this );
    advanced->setDisabled( true );
    advanced->setStyleSheet( "color:gray;" );
    advanced->hide();

    QHBoxLayout *fLyt = new QHBoxLayout();
    fLyt->addWidget( advanced );
    fLyt->addStretch();
    fLyt->addWidget( formatsLbl );
    fLyt->addWidget( formatsCB );

    /* Buttons */
    QPushButton *cancelBtn = new QPushButton( QIcon::fromTheme( "dialog-cancel" ), "&Cancel" );
    connect( cancelBtn, &QPushButton::clicked, this, &QDialog::reject );

    QPushButton *createBtn = new QPushButton( QIcon::fromTheme( "application-x-compressed-tar" ), "Create &Archive" );
    connect( createBtn, &QPushButton::clicked, this, &QDialog::accept );

    QHBoxLayout *bLyt = new QHBoxLayout();
    bLyt->addWidget( cancelBtn );
    bLyt->addStretch();
    bLyt->addWidget( createBtn );

    /* Final Layout */
    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->addLayout( nLyt );
    baseLyt->addLayout( lLyt );
    baseLyt->addLayout( fLyt );
    // baseLyt->addWidget( Separator::horizontal() );
    baseLyt->addLayout( bLyt );

    setLayout( baseLyt );
}


QString NewArchiveDialog::archive() {
    if ( filename.isEmpty() ) {
        return NULL;
    }

    QString archiveName = filename;

    if ( not filename.endsWith( format ) ) {
        archiveName += format;
    }

    return QDir( location ).filePath( archiveName );
}


void NewArchiveDialog::updateFileName( QString fn ) {
    filename = QString( fn );
}


void NewArchiveDialog::updateDirName() {
    QString loc = QFileDialog::getExistingDirectory(
        this,
        "DesQ Archiver | Choose Directory",
        QDir::currentPath()
    );

    if ( not loc.isEmpty() ) {
        locationLE->setText( loc );
        location = loc;
    }
}


void NewArchiveDialog::updateFormat( QString fmt ) {
    format = QString( fmt );
}


/**
 * Extract Archive Dialog
 **/
ExtractionSummary * ArchiveExtractDialog::extractArchive( QWidget *parent, QString fileName ) {
    ArchiveExtractDialog *dlg = new ArchiveExtractDialog( parent, fileName );

    if ( dlg->exec() == QDialog::Accepted ) {
        ExtractionSummary *summ = new ExtractionSummary {
            dlg->locationLE->text() + "/" + (dlg->subFolderCB->isChecked() ? dlg->subFolderLE->text() : ""),
            dlg->openDestCB->isChecked(),
            dlg->closeWinCB->isChecked(),
            dlg->preserveCB->isChecked(),
            (dlg->allFilesRB->isChecked() ? true : false),
        };

        return summ;
    }

    else {
        return NULL;
    }
}


ArchiveExtractDialog::ArchiveExtractDialog( QWidget *parent, QString fileName ) : QDialog( parent ) {
    createGUI();

    QString fn = DesQ::Utils::baseName( fileName.replace( LibArchiveQt::suffix( fileName ), "" ) );
    subFolderLE->setText( fn );

    setMinimumWidth( 450 );
}


void ArchiveExtractDialog::createGUI() {
    setWindowTitle( "DesQ Archiver | Extract Archive" );
    setWindowIcon( QIcon( ":/icons/desq-archiver.png" ) );

    /* Location Label */
    QLabel *locationLbl = new QLabel( "&Location:", this );

    locationLE = new QLineEdit( this );
    locationLE->setText( QDir::currentPath() );
    locationLE->setToolTip( "Click browse to select where the archive will be extracted" );
    locationLE->setDisabled( true );
    locationLbl->setBuddy( locationLE );

    QToolButton *locationTB = new QToolButton( this );
    locationTB->setIcon( QIcon::fromTheme( "folder" ) );
    locationLE->setToolTip( "Click to select where the archive will be stored" );
    connect( locationTB, &QToolButton::clicked, this, &ArchiveExtractDialog::updateDirName );

    QHBoxLayout *locLyt = new QHBoxLayout();
    locLyt->addWidget( locationLbl );
    locLyt->addWidget( locationLE );
    locLyt->addWidget( locationTB );

    subFolderCB = new QCheckBox( "&Extract to sub-folder:" );
    subFolderCB->setChecked( false );
    subFolderLE = new QLineEdit();
    subFolderLE->setDisabled( true );

    connect(
        subFolderCB, &QCheckBox::toggled, [ = ]( bool checked ) {
            subFolderLE->setEnabled( checked );
        }
    );

    connect(
        subFolderLE, &QLineEdit::textEdited, [ = ]( QString text ) {
            if ( text.contains( "/" ) ) {
                extractBtn->setDisabled( true );
                subFolderLE->setStyleSheet( "color: red;" );
            }

            else {
                extractBtn->setEnabled( true );
                subFolderLE->setStyleSheet( "" );
            }
        }
    );

    QHBoxLayout *subFolderLyt = new QHBoxLayout();
    subFolderLyt->addWidget( subFolderCB );
    subFolderLyt->addWidget( subFolderLE );

    openDestCB = new QCheckBox( "Open destination folder after extraction is complete" );
    openDestCB->setChecked( true );

    closeWinCB = new QCheckBox( "Close DesQ Archiver after extraction is complete" );
    closeWinCB->setChecked( true );

    preserveCB = new QCheckBox( "Preserve paths when extracting" );
    preserveCB->setChecked( true );
    preserveCB->setDisabled( true );
    preserveCB->setToolTip(
        "When extracting single members it is preferred to not to re-create the "
        "entire directory path. It's strongly advised to enable this when extracting all files."
    );

    QVBoxLayout *optsLyt = new QVBoxLayout();
    optsLyt->addWidget( new QLabel( "Options" ) );
    optsLyt->addWidget( openDestCB );
    optsLyt->addWidget( closeWinCB );
    optsLyt->addWidget( preserveCB );

    selectedRB = new QRadioButton( "Selected files only." );
    allFilesRB = new QRadioButton( "All files in the archive." );
    allFilesRB->setChecked( true );

    QVBoxLayout *extractLyt = new QVBoxLayout();
    extractLyt->addWidget( new QLabel( "Extract" ) );
    extractLyt->addWidget( selectedRB );
    extractLyt->addWidget( allFilesRB );

    /* Buttons */
    QPushButton *cancelBtn = new QPushButton( QIcon::fromTheme( "dialog-cancel" ), "&Cancel" );
    connect( cancelBtn, &QPushButton::clicked, this, &QDialog::reject );

    extractBtn = new QPushButton( QIcon::fromTheme( "archive-extract" ), "Extract &Archive" );
    connect(
        extractBtn, &QPushButton::clicked, [ = ]() {
            QString dest = locationLE->text() + "/" + (subFolderCB->isChecked() ? subFolderLE->text() : "");

            if ( DesQ::Utils::mkpath( dest ) ) {
                QMessageBox::critical(
                    this,
                    "DesQ Archiver | Error!",
                    "DesQ Archiver is unable to create the given path."
                );
            }

            else {
                QDialog::accept();
            }
        }
    );

    QHBoxLayout *bLyt = new QHBoxLayout();
    bLyt->addWidget( cancelBtn );
    bLyt->addStretch();
    bLyt->addWidget( extractBtn );

    /* Final Layout */
    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->addLayout( locLyt );
    baseLyt->addLayout( subFolderLyt );
    baseLyt->addLayout( optsLyt );
    baseLyt->addLayout( extractLyt );
    baseLyt->addLayout( bLyt );

    setLayout( baseLyt );
}


void ArchiveExtractDialog::updateDirName() {
    QString loc = QFileDialog::getExistingDirectory(
        this,
        "DesQ Archiver | Choose Directory",
        QDir::currentPath()
    );

    if ( not loc.isEmpty() ) {
        locationLE->setText( loc );
    }
}
