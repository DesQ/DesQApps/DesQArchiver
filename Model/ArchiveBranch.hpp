/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>
#include <QIcon>

class ArchiveBranch : public QObject {
    Q_OBJECT

    public:
        ArchiveBranch();
        ArchiveBranch( QString, QIcon, QString, ArchiveBranch *parent = 0 );

        int branchCount();

        void clearBranches();

        void addBranch( ArchiveBranch * );
        void removeBranch( ArchiveBranch * );

        ArchiveBranch * branch( int );
        ArchiveBranch * branch( QString );
        QList<ArchiveBranch *> branches();

        QVariant data( int role ) const;
        bool setData( int column, QVariant data );

        ArchiveBranch *parent();
        int row();

        void sort();

        QString name();
        QString size();
        QIcon icon();

    private:
        QList<ArchiveBranch *> mBranches;
        ArchiveBranch *parentNode;

        QString mPath;
        QIcon mIcon;
        QString mSize;

        QStringList __nameFilters;
        bool __showHidden;
};

bool caseInsensitiveNameSort( ArchiveBranch *first, ArchiveBranch *second );
