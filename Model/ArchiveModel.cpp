/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ArchiveModel.hpp"

#include <archive.h>
#include <archive_entry.h>
#include <desq/Utils.hpp>

static QMimeDatabase mimeDb;

/* Recursively add branches to the tree */
static void growBranch( QString path, ArchiveBranch *parent ) {
    if ( not QFileInfo( path ).exists() ) {
        return;
    }

    QDirIterator it( path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::NoIteratorFlags );
    while ( it.hasNext() ) {
        it.next();
        QFileInfo fi( it.fileInfo() );
        QString   name = fi.fileName();
        QString   size = (fi.isDir() ? "" : DesQ::Utils::formatSize( fi.size() ) );
        QMimeType mime = mimeDb.mimeTypeForFile( fi.filePath() );

        QString       iconName = mime.iconName();
        ArchiveBranch *branch  = new ArchiveBranch( name, QIcon::fromTheme( iconName ), size, parent );
        parent->addBranch( branch );

        if ( fi.isDir() ) {
            growBranch( it.filePath(), branch );
        }

        qApp->processEvents();
    }
}


/*
 * Walk all the nodes of a branch: branchpath is appended to all subsequent nodes
 * @parent will not be a part of this list, also folders will not be listed
 */
static QStringList walkTree( QString branchPath, ArchiveBranch *parent ) {
    QStringList fileList;

    QDir curBranch( branchPath );

    for ( ArchiveBranch *branch: parent->branches() ) {
        /* If this branch is further branched out - it is a folder */
        if ( branch->branchCount() ) {
            fileList << walkTree( curBranch.filePath( branch->name() ), branch );
        }

        else {
            fileList << curBranch.filePath( branch->name() );
        }

        qApp->processEvents();
    }

    return fileList;
}


ArchiveModel::ArchiveModel() : QAbstractItemModel() {
    /* Root node */
    tree = new ArchiveBranch( "Name", QIcon::fromTheme( "folder" ), 0 );
}


void ArchiveModel::setArchiveName( QString path ) {
    /* Archive name and path */
    archiveName = path;
}


void ArchiveModel::loadArchive( QString filename ) {
    /* Archive name and path */
    archiveName = filename;

    /* Root node */
    growTree();
}


int ArchiveModel::rowCount( const QModelIndex& parent ) const {
    ArchiveBranch *parentItem;

    if ( parent.column() > 0 ) {
        return 0;
    }

    if ( not parent.isValid() ) {
        parentItem = tree;
    }

    else {
        parentItem = static_cast<ArchiveBranch *>(parent.internalPointer() );
    }

    return parentItem->branchCount();
}


int ArchiveModel::columnCount( const QModelIndex& ) const {
    return 2;
}


Qt::ItemFlags ArchiveModel::flags( const QModelIndex& idx ) const {
    if ( not idx.isValid() ) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}


QVariant ArchiveModel::data( const QModelIndex& index, int role ) const {
    if ( not index.isValid() ) {
        return QVariant();
    }

    ArchiveBranch *node = static_cast<ArchiveBranch *>(index.internalPointer() );
    switch ( role ) {
        case Qt::DisplayRole: {
            if ( index.column() == 0 ) {
                return node->name();
            }

            else if ( index.column() == 1 ) {
                return node->size();
            }

            return QVariant();
        }

        case Qt::DecorationRole: {
            if ( index.column() == 0 ) {
                return node->icon();
            }

            return QVariant();
        }

        case Qt::TextAlignmentRole: {
            if ( index.column() == 0 ) {
                return (0x0001 | 0x0080);
            }

            else if ( index.column() == 1 ) {
                return (0x0002 | 0x0080);
            }

            else {
                return Qt::AlignCenter;
            }
        }

        case Qt::InitialSortOrderRole: {
            return Qt::AscendingOrder;
        }

        case Qt::AccessibleTextRole: {
            return node->name();
        }

        case Qt::ToolTipRole: {
            if ( index.column() == 0 ) {
                return node->data( Qt::UserRole + 1 );
            }

            return QString();
        }

        case Qt::UserRole + 1: {
            return node->data( Qt::UserRole + 1 );
        }

        default: {
            return QVariant();
        }
    }
}


QVariant ArchiveModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( orientation != Qt::Horizontal ) {
        return QVariant();
    }

    switch ( section ) {
        case 0: {
            switch ( role ) {
                case Qt::DisplayRole: {
                    return "Name";
                }

                case Qt::TextAlignmentRole: {
                    return Qt::AlignCenter;
                }

                default: {
                    return QVariant();
                }
            }
        }

        case 1: {
            switch ( role ) {
                case Qt::DisplayRole: {
                    return "Size";
                }

                case Qt::TextAlignmentRole: {
                    return Qt::AlignCenter;
                }

                default: {
                    return QVariant();
                }
            }
        }

        default: {
            return QVariant();
        }
    }
}


QModelIndex ArchiveModel::index( int row, int column, const QModelIndex& parent ) const {
    if ( row < 0 or column < 0 ) {
        return QModelIndex();
    }

    if ( (row > rowCount( parent ) ) and (column > columnCount( parent ) ) ) {
        return QModelIndex();
    }

    ArchiveBranch *parentNode;

    if ( not parent.isValid() ) {
        parentNode = tree;
    }

    else {
        parentNode = (ArchiveBranch *)parent.internalPointer();
    }

    ArchiveBranch *branch = parentNode->branch( row );

    if ( branch ) {
        return createIndex( row, column, branch );
    }

    else {
        return QModelIndex();
    }
}


QModelIndex ArchiveModel::index( QString name, const QModelIndex& parent ) const {
    ArchiveBranch *parentNode;

    if ( not parent.isValid() ) {
        parentNode = tree;
    }

    else {
        parentNode = static_cast<ArchiveBranch *>(parent.internalPointer() );
    }

    ArchiveBranch *branch = parentNode->branch( name );

    if ( branch ) {
        return createIndex( branch->row(), 0, branch );
    }

    return QModelIndex();
}


QModelIndex ArchiveModel::parent( const QModelIndex& idx ) const {
    if ( not idx.isValid() ) {
        return QModelIndex();
    }

    ArchiveBranch *branch     = ( ArchiveBranch * )idx.internalPointer();
    ArchiveBranch *parentNode = branch->parent();

    if ( parentNode == tree ) {
        return QModelIndex();
    }

    return createIndex( parentNode->row(), 0, parentNode );
}


bool ArchiveModel::hasBranches( const QModelIndex& idx ) const {
    if ( not idx.isValid() ) {
        return true;
    }

    else {
        ArchiveBranch *branch = static_cast<ArchiveBranch *>(idx.internalPointer() );
        return (branch->branchCount() > 0);
    }
}


QString ArchiveModel::nodeName( const QModelIndex idx ) const {
    return idx.data( 0 ).toString();
}


QString ArchiveModel::nodePath( const QModelIndex idx ) const {
    if ( not idx.isValid() ) {
        return QString();
    }

    QString     path = idx.data().toString();
    QModelIndex par  = parent( idx );

    while ( par.isValid() ) {
        path = par.data().toString() + "/" + path;
        par  = parent( par );
    }

    return path;
}


void ArchiveModel::growBranch( QStringList files ) {
    beginResetModel();
    emit archiveLoading();

    Q_FOREACH ( QString file, files ) {
        QFileInfo fi( file );

        QString   name = fi.fileName();
        QString   size = (fi.isDir() ? "" : DesQ::Utils::formatSize( fi.size() ) );
        QMimeType mime = mimeDb.mimeTypeForFile( fi.filePath() );

        QString       iconName = mime.iconName();
        ArchiveBranch *branch  = new ArchiveBranch( file, QIcon::fromTheme( iconName ), size, tree );
        tree->addBranch( branch );

        if ( fi.isDir() ) {
            ::growBranch( file, branch );
        }
    }

    mModified = true;
    emit archiveLoaded();

    endResetModel();
}


void ArchiveModel::chopBranch( QModelIndexList indexes ) {
    emit archiveLoading();

    Q_FOREACH ( QModelIndex idx, indexes ) {
        beginRemoveRows( idx.parent(), idx.row(), idx.row() );

        ArchiveBranch *parent;
        ArchiveBranch *branch = (ArchiveBranch *)idx.internalPointer();

        if ( branch ) {
            parent = branch->parent();
        }

        else {
            continue;
        }

        parent->removeBranch( branch );

        endRemoveRows();
    }

    mModified = true;
    emit archiveLoaded();
}


bool ArchiveModel::isArchiveModified() {
    return mModified;
}


QStringList ArchiveModel::filePaths() {
    return walkTree( QString(), tree );
}


void ArchiveModel::growTree() {
    beginResetModel();

    tree->clearBranches();

    if ( DesQ::Utils::exists( archiveName ) ) {
        LibArchiveQt *archive = new LibArchiveQt( archiveName );

        emit archiveLoading();
        Q_FOREACH ( ArchiveEntry *ae, archive->listArchive() ) {
            QString name = QString( ae->name );
            int     type = ae->type;
            QString size = DesQ::Utils::formatSize( ae->size );

            QStringList tokens   = QStringList() << name.split( "/", Qt::SkipEmptyParts );
            QString     iconName = ( (type == AE_IFDIR) ? "folder" : mimeDb.mimeTypeForFile( tokens.value( 0 ) ).iconName() );
            tree->addBranch(
                new ArchiveBranch( tokens.value( 0 ), QIcon::fromTheme( iconName ), ( (type == AE_IFDIR) ? "" : size), tree )
            );

            if ( tokens.size() == 1 ) {
                continue;
            }

            ArchiveBranch *branch = tree;
            for ( int i = 1; i < tokens.size(); i++ ) {
                branch = branch->branch( tokens.value( i - 1 ) );
                QString iconName = ( (type == AE_IFDIR) ? "folder" : mimeDb.mimeTypeForFile( tokens.value( i ) ).iconName() );

                branch->addBranch(
                    new ArchiveBranch( tokens.value( i ), QIcon::fromTheme( iconName ), ( (type == AE_IFDIR) ? "" : size), branch )
                );
            }

            qApp->processEvents();
        }

        emit archiveLoaded();
    }

    endResetModel();
}
