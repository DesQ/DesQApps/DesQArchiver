/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ArchiveBranch.hpp"

ArchiveBranch::ArchiveBranch() : QObject() {
    mPath = QString();
    mIcon = QIcon();

    parentNode = 0;
}


ArchiveBranch::ArchiveBranch( QString name, QIcon icon, QString size, ArchiveBranch *parent ) : QObject() {
    mPath = QString( name );
    mIcon = icon;
    mSize = size;

    parentNode = parent;
}


int ArchiveBranch::branchCount() {
    return mBranches.count();
}


void ArchiveBranch::clearBranches() {
    mBranches.clear();
}


void ArchiveBranch::addBranch( ArchiveBranch *newNode ) {
    Q_FOREACH ( ArchiveBranch *node, mBranches ) {
        if ( node->name() == newNode->name() ) {
            return;
        }
    }

    mBranches << newNode;
}


void ArchiveBranch::removeBranch( ArchiveBranch *node ) {
    delete mBranches.takeAt( node->row() );
}


ArchiveBranch * ArchiveBranch::branch( int row ) {
    return mBranches.at( row );
}


ArchiveBranch * ArchiveBranch::branch( QString name ) {
    Q_FOREACH ( ArchiveBranch *node, mBranches ) {
        if ( node->name() == name ) {
            return node;
        }
    }

    return new ArchiveBranch();
}


QList<ArchiveBranch *> ArchiveBranch::branches() {
    return mBranches;
}


QString ArchiveBranch::name() {
    return mPath;
}


QString ArchiveBranch::size() {
    return mSize;
}


QIcon ArchiveBranch::icon() {
    /** If this branch has children, then it's a folder. */
    if ( mBranches.count() ) {
        return QIcon::fromTheme( "folder" );
    }

    /** Otherwise, we use the icon given to us. */
    return mIcon;
}


QVariant ArchiveBranch::data( int role ) const {
    switch ( role ) {
        case Qt::DisplayRole: {
            return mPath;
        }

        case Qt::DecorationRole: {
            return mIcon;
        }

        case Qt::UserRole + 1: {
            return mPath;
        }

        default: {
            return QVariant();
        }
    }
}


bool ArchiveBranch::setData( int, QVariant ) {
    return true;
}


ArchiveBranch * ArchiveBranch::parent() {
    return parentNode;
}


int ArchiveBranch::row() {
    /* If mPath is not defined */
    if ( not mPath.length() ) {
        return -1;
    }

    if ( parentNode ) {
        return parentNode->mBranches.indexOf( this );
    }

    return 0;
}


void ArchiveBranch::sort() {
    std::sort( mBranches.begin(), mBranches.end(), caseInsensitiveNameSort );
}


bool caseInsensitiveNameSort( ArchiveBranch *first, ArchiveBranch *second ) {
    QString name1 = first->name().toLower();
    QString name2 = second->name().toLower();

    return name1 < name2;
}
