/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDebug>
#include <QMainWindow>

#include <DFApplication.hpp>
#include <DFSettings.hpp>

#include "DesQArchiver.hpp"
#include <libarchiveqt.h>

int main( int argc, char *argv[] ) {
    DFL::Application app( argc, argv );
    app.setQuitOnLastWindowClosed( true );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Archiver" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-archiver" );

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    /* Optional: Flag to create an archive */
    parser.addOption( { "create", "Create a new archive", "archive" } );

    /* Optional: Flag to extract an archive */
    parser.addOption( { "extract", "Extract an archive", "archive" } );

    /* Optional: Don't show GUI. Only cli */
    parser.addOption( { "no-gui", "Don't show an UI" } );

    /* Optional: */
    parser.addPositionalArgument( "files|members", "Files/members that you want to add|extract", "files|members" );

    /* Process the CLI args */
    parser.process( app );

    QStringList posArgs = parser.positionalArguments();

    /* Don't show GUI */
    if ( parser.isSet( "no-gui" ) ) {
        if ( parser.isSet( "create" ) ) {
            if ( posArgs.count() ) {
                LibArchiveQt *archive = new LibArchiveQt( parser.value( "create" ) );
                QObject::connect(
                    archive, &LibArchiveQt::jobFailed, [ = ]() {
                        qDebug() << "[ERROR]: DesQ Archiver encountered some errors while creating the archive. ";
                        qDebug() << "[ERROR]: All the files may not have been added to the archive.";
                        return 1;
                    }
                );

                archive->updateInputFiles( posArgs, LibArchiveQt::CommonRelativePath );
                archive->createArchive();
                archive->waitForFinished();

                return 0;
            }
        }

        else if ( parser.isSet( "extract" ) ) {
            // Read archive code
            LibArchiveQt *arc = new LibArchiveQt( parser.value( "extract" ) );

            /* Extract members */
            if ( posArgs.count() ) {
                QString curMember;
                int     errors = 0;
                QObject::connect(
                    arc, &LibArchiveQt::jobFailed, [ = ]() mutable {
                        qDebug() << "[ERROR]: DesQ Archiver encountered an error while extracting " << curMember;
                        errors++;
                    }
                );

                for ( QString arg: posArgs ) {
                    curMember = arg;
                    arc->extractMember( curMember );
                    arc->waitForFinished();
                }

                if ( errors ) {
                    return 1;
                }

                else {
                    return 0;
                }
            }

            /* Extract everything */
            else {
                QObject::connect(
                    arc, &LibArchiveQt::jobFailed, [ = ]() {
                        qDebug() << "[ERROR]: DesQ Archiver encountered some errors while extracting the archive.";
                        qDebug() << "[ERROR]: All the files may not have been extracted properly.";
                        return 1;
                    }
                );

                arc->extractArchive();
                arc->waitForFinished();
            }
        }
    }

    /* Show the GUI */
    else {
        QString command;
        QString arcName;

        /* --create <AN> was specified */
        if ( parser.isSet( "create" ) ) {
            command = "create";
            arcName = parser.value( "create" );
        }

        /* --extract <AN> was specified */
        else if ( parser.isSet( "extract" ) ) {
            command = "extract";
            arcName = parser.value( "extract" );
        }

        /* --create/extract not specified */
        else {
            command = "auto";
            arcName = "";
        }

        DesQArchiver *archiver = new DesQArchiver();
        archiver->show();

        qApp->processEvents();

        /* If we have an archive name, put it at posArgs[ 0 ] */
        if ( not arcName.isEmpty() ) {
            posArgs.prepend( arcName );
        }

        /* If we have any positional arguments, load them */
        if ( posArgs.count() ) {
            archiver->load( command, posArgs );
        }
    }

    return app.exec();
}
