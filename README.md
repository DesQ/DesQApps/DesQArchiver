# DesQ Archiver
## A light-weight archiver for the DesQ Desktop Environment

This is an app to create, view, extract and modify archive. It is built to run smoothly on devices with different form factors.
Some of its features are

- Drag and drop files and folders into a new archive
- Inbuilt filesystem browser


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)
* libarchive-qt (https://gitlab.com/marcusbritanicus/libarchive-qt)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQArchiver.git DesQArchiver`
- Enter the `DesQArchiver` folder
  * `cd DesQArchiver`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* Open archives in tabs
* Any other feature you request for... :)
